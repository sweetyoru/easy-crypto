# 1. What is this?

This is a crate for quickly write some code using crypto.


# 2. Why?

I find current state of rust-crypto is not easy to use, at least for me.


# 3. Current state of development:

- Block cipher:
	- Aes128: done
	- Aes192: done
	- Aes256: done

- Block mode:
	- ECB: done
	- CBC: done
	- OFB: done
	- CTR: done

- Hash function:
	- md5: done
	- sha1: done
	- sha2-224: done
	- sha2-256: done
	- sha2-384: done
	- sha2-512: done
	- sha3-224
	- sha3-256
	- sha3-384
	- sha3-512


**Pull request and bug report are welcome.**
