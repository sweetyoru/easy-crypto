use std::num::Wrapping;

use crate::Misc;
use crate::Hasher::HasherTrait;


#[derive(Debug, Clone)]
pub struct Sha1
{
	h0: Wrapping<u32>,
	h1: Wrapping<u32>,
	h2: Wrapping<u32>,
	h3: Wrapping<u32>,
	h4: Wrapping<u32>,
	leftoverInput: Vec<u8>,
	inputLen: u64,
}

impl Sha1
{
	const BLOCK_SIZE: usize = 64;
	const ROUND_COUNT: usize = 80;

	pub fn New () -> Self
	{
		Self
		{
			h0: Wrapping(0x67452301),
			h1: Wrapping(0xEFCDAB89),
			h2: Wrapping(0x98BADCFE),
			h3: Wrapping(0x10325476),
			h4: Wrapping(0xC3D2E1F0),
			leftoverInput: Vec::new (),
			inputLen: 0,
		}
	}


	fn AddBlock (&mut self, input: &[u8; Self::BLOCK_SIZE])
	{
		let mut w = [0u32; 80];

		// make w[0..16]
		for i in (0..input.len ()).step_by (4)
		{
			w[i/4] = u32::from_be_bytes (input[i..i + 4].try_into ().unwrap ());
		}

		// make w[16..80]
		for i in 16..80
		{
			w[i] = (w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16]).rotate_left (1);
		}

		// init values
		let mut a = self.h0;
		let mut b = self.h1;
		let mut c = self.h2;
		let mut d = self.h3;
		let mut e = self.h4;

		// do 80 rounds
		for i in 0..Self::ROUND_COUNT
		{
			let (f, k) = match i
			{
				x if (0..20).contains (&x) =>
				{
					((b & c) | ((!b) & d), Wrapping(0x5A827999))
				},
				x if (20..40).contains (&x) =>
				{
					(b ^ c ^ d, Wrapping(0x6ED9EBA1))
				},
				x if (40..60).contains (&x) =>
				{
					((b & c) | (b & d) | (c & d), Wrapping(0x8F1BBCDC))
				},
				x if (60..80).contains (&x) =>
				{
					(b ^ c ^ d, Wrapping(0xCA62C1D6))
				},
				_ => panic! ("Something is wrong"),
			};


			let temp = a.rotate_left (5) + f + e + k + Wrapping(w[i]);
			e = d;
			d = c;
			c = b.rotate_left (30);
			b = a;
			a = temp;
		}


		// add this block hash to current hash
		self.h0 += a;
		self.h1 += b;
		self.h2 += c;
		self.h3 += d;
		self.h4 += e;
	}

	pub fn AddInput (&mut self, input: &[u8])
	{
		if self.leftoverInput.len () + input.len () < Self::BLOCK_SIZE
		{
			// not enough byte to process
			self.leftoverInput.extend (input);

			// update input len
			self.inputLen += u64::try_from (input.len ()).unwrap ();
			return;
		}

		// first block
		let buffer = [&self.leftoverInput, &input[0..Self::BLOCK_SIZE - self.leftoverInput.len ()]].concat ();
		assert! (buffer.len () == Self::BLOCK_SIZE);
		self.AddBlock (&buffer[0..Self::BLOCK_SIZE].try_into ().unwrap ());

		// do next n blocks
		for block in input[Self::BLOCK_SIZE - self.leftoverInput.len ()..].chunks_exact (Self::BLOCK_SIZE)
		{
			self.AddBlock (block.try_into ().unwrap ());
		}

		// push last block into leftover

		// .......................................
		// |      |      |      |      |      |

		// .......................................
		//     |      |      |      |      |      |

		// .......................................
		//   |      |      |      |      |      |
		let x = Misc::AlignLeft::<usize> (input.len (), Self::BLOCK_SIZE);
		let mut y = x + (Self::BLOCK_SIZE - self.leftoverInput.len ());

		if y > input.len ()
		{
			y -= Self::BLOCK_SIZE;
		}

		self.leftoverInput.clear ();
		self.leftoverInput.extend (&input[y..]);

		// update input len
		self.inputLen += u64::try_from (input.len ()).unwrap ();
	}

	pub fn GetOutput (mut self) -> [u8; Self::OUTPUT_SIZE]
	{
		let paddingLen = Misc::Align::<u64> (self.inputLen + 1 + 8, Self::BLOCK_SIZE) - (self.inputLen + 1 + 8);
		let lastBlock = [&[0x80][..], &vec! [0x00; paddingLen.try_into ().unwrap ()], &(self.inputLen*8).to_be_bytes ()].concat ();
		self.AddInput (&lastBlock);

		[self.h0.0.to_be_bytes (), self.h1.0.to_be_bytes (), self.h2.0.to_be_bytes (), self.h3.0.to_be_bytes (), self.h4.0.to_be_bytes ()].concat ().try_into ().unwrap ()
	}

	pub fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		let mut hasher = Self::New ();
		hasher.AddInput (input);
		hasher.GetOutput ()
	}
}


impl HasherTrait for Sha1
{
	const OUTPUT_SIZE: usize = 20;

	fn AddInput (&mut self, input: &[u8])
	{
		self.AddInput (input);
	}

	fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.GetOutput ()
	}

	fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Self::Hash (input)
	}
}


#[derive(Debug, Clone)]
pub struct Sha2_224
{
	coreSha2_256: Sha2_256,
}

impl Sha2_224
{
	pub fn New () -> Self
	{
		Self
		{
			coreSha2_256: Sha2_256
			{
				h0: Wrapping(0xc1059ed8),
				h1: Wrapping(0x367cd507),
				h2: Wrapping(0x3070dd17),
				h3: Wrapping(0xf70e5939),
				h4: Wrapping(0xffc00b31),
				h5: Wrapping(0x68581511),
				h6: Wrapping(0x64f98fa7),
				h7: Wrapping(0xbefa4fa4),
				leftoverInput: Vec::new (),
				inputLen: 0,
			},
		}
	}


	pub fn AddInput (&mut self, input: &[u8])
	{
		self.coreSha2_256.AddInput (input);
	}

	pub fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.coreSha2_256.GetOutput ()[0..Self::OUTPUT_SIZE].try_into ().unwrap ()
	}

	pub fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Sha2_256::Hash (input)[0..Self::OUTPUT_SIZE].try_into ().unwrap ()
	}
}


impl HasherTrait for Sha2_224
{
	const OUTPUT_SIZE: usize = 28;

	fn AddInput (&mut self, input: &[u8])
	{
		self.AddInput (input);
	}

	fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.GetOutput ()
	}

	fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Self::Hash (input)
	}
}


#[derive(Debug, Clone)]
pub struct Sha2_256
{
	h0: Wrapping<u32>,
	h1: Wrapping<u32>,
	h2: Wrapping<u32>,
	h3: Wrapping<u32>,
	h4: Wrapping<u32>,
	h5: Wrapping<u32>,
	h6: Wrapping<u32>,
	h7: Wrapping<u32>,
	leftoverInput: Vec<u8>,
	inputLen: u64,
}

impl Sha2_256
{
	const BLOCK_SIZE: usize = 64;
	const ROUND_COUNT: usize = 64;

	const ROUND_CONSTANTS: [u32; 64] =
	[
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	];

	pub fn New () -> Self
	{
		Self
		{
			h0: Wrapping(0x6a09e667),
			h1: Wrapping(0xbb67ae85),
			h2: Wrapping(0x3c6ef372),
			h3: Wrapping(0xa54ff53a),
			h4: Wrapping(0x510e527f),
			h5: Wrapping(0x9b05688c),
			h6: Wrapping(0x1f83d9ab),
			h7: Wrapping(0x5be0cd19),
			leftoverInput: Vec::new (),
			inputLen: 0,
		}
	}


	fn AddBlock (&mut self, input: &[u8; Self::BLOCK_SIZE])
	{
		let mut w = [Wrapping(0u32); 64];

		// make w[0..16]
		for i in (0..input.len ()).step_by (4)
		{
			w[i/4] = Wrapping(u32::from_be_bytes (input[i..i + 4].try_into ().unwrap ()));
		}

		// make w[16..64]
		for i in 16..64
		{
			let s0 = w[i - 15].rotate_right (7) ^ w[i - 15].rotate_right (18) ^ (w[i - 15] >> 3);
			let s1 = w[i - 2].rotate_right (17) ^ w[i - 2].rotate_right (19) ^ (w[i - 2] >> 10);
			w[i] = w[i - 16] + s0 + w[i - 7] + s1;
		}

		// init values
		let mut a = self.h0;
		let mut b = self.h1;
		let mut c = self.h2;
		let mut d = self.h3;
		let mut e = self.h4;
		let mut f = self.h5;
		let mut g = self.h6;
		let mut h = self.h7;

		// do 64 rounds
		for i in 0..Self::ROUND_COUNT
		{
			let s0 = a.rotate_right (2) ^ a.rotate_right (13) ^ a.rotate_right (22);
			let s1 = e.rotate_right (6) ^ e.rotate_right (11) ^ e.rotate_right (25);

			let ch = (e & f) ^ ((!e) & g);
			let temp1 = h + s1 + ch + Wrapping(Self::ROUND_CONSTANTS[i]) + w[i];

			let maj = (a & b) ^ (a & c) ^ (b & c);
			let temp2 = s0 + maj;

			h = g;
			g = f;
			f = e;
			e = d + temp1;
			d = c;
			c = b;
			b = a;
			a = temp1 + temp2;
		}


		// add this block hash to current hash
		self.h0 += a;
		self.h1 += b;
		self.h2 += c;
		self.h3 += d;
		self.h4 += e;
		self.h5 += f;
		self.h6 += g;
		self.h7 += h;
	}

	pub fn AddInput (&mut self, input: &[u8])
	{
		if self.leftoverInput.len () + input.len () < Self::BLOCK_SIZE
		{
			// not enough byte to process
			self.leftoverInput.extend (input);

			// update input len
			self.inputLen += u64::try_from (input.len ()).unwrap ();
			return;
		}

		// first block
		let buffer = [&self.leftoverInput, &input[0..Self::BLOCK_SIZE - self.leftoverInput.len ()]].concat ();
		assert! (buffer.len () == Self::BLOCK_SIZE);
		self.AddBlock (&buffer[0..Self::BLOCK_SIZE].try_into ().unwrap ());

		// do next n blocks
		for block in input[Self::BLOCK_SIZE - self.leftoverInput.len ()..].chunks_exact (Self::BLOCK_SIZE)
		{
			self.AddBlock (block.try_into ().unwrap ());
		}

		// push last block into leftover

		// .......................................
		// |      |      |      |      |      |

		// .......................................
		//     |      |      |      |      |      |

		// .......................................
		//   |      |      |      |      |      |
		let x = Misc::AlignLeft::<usize> (input.len (), Self::BLOCK_SIZE);
		let mut y = x + (Self::BLOCK_SIZE - self.leftoverInput.len ());

		if y > input.len ()
		{
			y -= Self::BLOCK_SIZE;
		}

		self.leftoverInput.clear ();
		self.leftoverInput.extend (&input[y..]);

		// update input len
		self.inputLen += u64::try_from (input.len ()).unwrap ();
	}

	pub fn GetOutput (mut self) -> [u8; Self::OUTPUT_SIZE]
	{
		let paddingLen = Misc::Align::<u64> (self.inputLen + 1 + 8, Self::BLOCK_SIZE) - (self.inputLen + 1 + 8);
		let lastBlock = [&[0x80][..], &vec! [0x00; paddingLen.try_into ().unwrap ()], &(self.inputLen*8).to_be_bytes ()].concat ();
		self.AddInput (&lastBlock);

		[self.h0.0.to_be_bytes (), self.h1.0.to_be_bytes (), self.h2.0.to_be_bytes (), self.h3.0.to_be_bytes (), self.h4.0.to_be_bytes (), self.h5.0.to_be_bytes (), self.h6.0.to_be_bytes (), self.h7.0.to_be_bytes ()].concat ().try_into ().unwrap ()
	}

	pub fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		let mut hasher = Self::New ();
		hasher.AddInput (input);
		hasher.GetOutput ()
	}
}


impl HasherTrait for Sha2_256
{
	const OUTPUT_SIZE: usize = 32;

	fn AddInput (&mut self, input: &[u8])
	{
		self.AddInput (input);
	}

	fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.GetOutput ()
	}

	fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Self::Hash (input)
	}
}


#[derive(Debug, Clone)]
pub struct Sha2_384
{
	coreSha2_512: Sha2_512,
}

impl Sha2_384
{
	pub fn New () -> Self
	{
		Self
		{
			coreSha2_512: Sha2_512
			{
				h0: Wrapping(0xcbbb9d5dc1059ed8),
				h1: Wrapping(0x629a292a367cd507),
				h2: Wrapping(0x9159015a3070dd17),
				h3: Wrapping(0x152fecd8f70e5939),
				h4: Wrapping(0x67332667ffc00b31),
				h5: Wrapping(0x8eb44a8768581511),
				h6: Wrapping(0xdb0c2e0d64f98fa7),
				h7: Wrapping(0x47b5481dbefa4fa4),
				leftoverInput: Vec::new (),
				inputLen: 0,
			},
		}
	}


	pub fn AddInput (&mut self, input: &[u8])
	{
		self.coreSha2_512.AddInput (input);
	}

	pub fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.coreSha2_512.GetOutput ()[0..Self::OUTPUT_SIZE].try_into ().unwrap ()
	}

	pub fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Sha2_512::Hash (input)[0..Self::OUTPUT_SIZE].try_into ().unwrap ()
	}
}


impl HasherTrait for Sha2_384
{
	const OUTPUT_SIZE: usize = 48;

	fn AddInput (&mut self, input: &[u8])
	{
		self.AddInput (input);
	}

	fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.GetOutput ()
	}

	fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Self::Hash (input)
	}
}


#[derive(Debug, Clone)]
pub struct Sha2_512
{
	h0: Wrapping<u64>,
	h1: Wrapping<u64>,
	h2: Wrapping<u64>,
	h3: Wrapping<u64>,
	h4: Wrapping<u64>,
	h5: Wrapping<u64>,
	h6: Wrapping<u64>,
	h7: Wrapping<u64>,
	leftoverInput: Vec<u8>,
	inputLen: u128,
}

impl Sha2_512
{
	const BLOCK_SIZE: usize = 128;
	const ROUND_COUNT: usize = 80;

	const ROUND_CONSTANTS: [u64; 80] =
	[
		0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
		0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
		0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
		0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
		0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
		0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
		0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
		0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
		0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
		0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
		0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
		0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
		0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
		0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
		0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
		0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
		0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
		0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
		0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
		0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
	];

	pub fn New () -> Self
	{
		Self
		{
			h0: Wrapping(0x6a09e667f3bcc908),
			h1: Wrapping(0xbb67ae8584caa73b),
			h2: Wrapping(0x3c6ef372fe94f82b),
			h3: Wrapping(0xa54ff53a5f1d36f1),
			h4: Wrapping(0x510e527fade682d1),
			h5: Wrapping(0x9b05688c2b3e6c1f),
			h6: Wrapping(0x1f83d9abfb41bd6b),
			h7: Wrapping(0x5be0cd19137e2179),
			leftoverInput: Vec::new (),
			inputLen: 0,
		}
	}


	fn AddBlock (&mut self, input: &[u8; Self::BLOCK_SIZE])
	{
		let mut w = [Wrapping(0u64); 80];

		// make w[0..16]
		for i in (0..input.len ()).step_by (8)
		{
			w[i/8] = Wrapping(u64::from_be_bytes (input[i..i + 8].try_into ().unwrap ()));
		}

		// make w[16..80]
		for i in 16..80
		{
			let s0 = w[i - 15].rotate_right (1) ^ w[i-15].rotate_right (8) ^ (w[i - 15] >> 7);
			let s1 = w[i - 2].rotate_right (19) ^ w[i-2].rotate_right (61) ^ (w[i - 2] >> 6);
			w[i] = w[i - 16] + s0 + w[i - 7] + s1;
		}

		// init values
		let mut a = self.h0;
		let mut b = self.h1;
		let mut c = self.h2;
		let mut d = self.h3;
		let mut e = self.h4;
		let mut f = self.h5;
		let mut g = self.h6;
		let mut h = self.h7;

		// do 80 rounds
		for i in 0..Self::ROUND_COUNT
		{
			let s0 = a.rotate_right (28) ^ a.rotate_right (34) ^ a.rotate_right (39);
			let s1 = e.rotate_right (14) ^ e.rotate_right (18) ^ e.rotate_right (41);

			let ch = (e & f) ^ ((!e) & g);
			let temp1 = h + s1 + ch + Wrapping(Self::ROUND_CONSTANTS[i]) + w[i];

			let maj = (a & b) ^ (a & c) ^ (b & c);
			let temp2 = s0 + maj;

			h = g;
			g = f;
			f = e;
			e = d + temp1;
			d = c;
			c = b;
			b = a;
			a = temp1 + temp2;
		}


		// add this block hash to current hash
		self.h0 += a;
		self.h1 += b;
		self.h2 += c;
		self.h3 += d;
		self.h4 += e;
		self.h5 += f;
		self.h6 += g;
		self.h7 += h;
	}
	pub fn AddInput (&mut self, input: &[u8])
	{
		if self.leftoverInput.len () + input.len () < Self::BLOCK_SIZE
		{
			// not enough byte to process
			self.leftoverInput.extend (input);

			// update input len
			self.inputLen += u128::try_from (input.len ()).unwrap ();
			return;
		}

		// first block
		let buffer = [&self.leftoverInput, &input[0..Self::BLOCK_SIZE - self.leftoverInput.len ()]].concat ();
		assert! (buffer.len () == Self::BLOCK_SIZE);
		self.AddBlock (&buffer[0..Self::BLOCK_SIZE].try_into ().unwrap ());

		// do next n blocks
		for block in input[Self::BLOCK_SIZE - self.leftoverInput.len ()..].chunks_exact (Self::BLOCK_SIZE)
		{
			self.AddBlock (block.try_into ().unwrap ());
		}

		// push last block into leftover

		// .......................................
		// |      |      |      |      |      |

		// .......................................
		//     |      |      |      |      |      |

		// .......................................
		//   |      |      |      |      |      |
		let x = Misc::AlignLeft::<usize> (input.len (), Self::BLOCK_SIZE);
		let mut y = x + (Self::BLOCK_SIZE - self.leftoverInput.len ());

		if y > input.len ()
		{
			y -= Self::BLOCK_SIZE;
		}

		self.leftoverInput.clear ();
		self.leftoverInput.extend (&input[y..]);

		// update input len
		self.inputLen += u128::try_from (input.len ()).unwrap ();
	}

	pub fn GetOutput (mut self) -> [u8; Self::OUTPUT_SIZE]
	{
		let paddingLen = Misc::Align::<u128> (self.inputLen + 1 + 16, Self::BLOCK_SIZE) - (self.inputLen + 1 + 16);
		let lastBlock = [&[0x80][..], &vec! [0x00; paddingLen.try_into ().unwrap ()], &(self.inputLen*8).to_be_bytes ()].concat ();
		self.AddInput (&lastBlock);

		[self.h0.0.to_be_bytes (), self.h1.0.to_be_bytes (), self.h2.0.to_be_bytes (), self.h3.0.to_be_bytes (), self.h4.0.to_be_bytes (), self.h5.0.to_be_bytes (), self.h6.0.to_be_bytes (), self.h7.0.to_be_bytes ()].concat ().try_into ().unwrap ()
	}

	pub fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		let mut hasher = Self::New ();
		hasher.AddInput (input);
		hasher.GetOutput ()
	}
}


impl HasherTrait for Sha2_512
{
	const OUTPUT_SIZE: usize = 64;

	fn AddInput (&mut self, input: &[u8])
	{
		self.AddInput (input);
	}

	fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE]
	{
		self.GetOutput ()
	}

	fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE]
	{
		Self::Hash (input)
	}
}


pub struct Sha3_224;
pub struct Sha3_256;
pub struct Sha3_384;
pub struct Sha3_512;


#[cfg(test)]
mod tests
{
	use super::*;
	use hex;


	#[test]
	fn Sha1_1 ()
	{
		let sha1 = Sha1::New ();
		let output = sha1.GetOutput ();
		assert! (hex::encode (output) == "da39a3ee5e6b4b0d3255bfef95601890afd80709");
	}

	#[test]
	fn Sha1_2 ()
	{
		let mut sha1 = Sha1::New ();
		sha1.AddInput ("abc".as_bytes ());
		let output = sha1.GetOutput ();
		assert! (hex::encode (output) == "a9993e364706816aba3e25717850c26c9cd0d89d");
	}

	#[test]
	fn Sha1_3 ()
	{
		let mut sha1 = Sha1::New ();
		sha1.AddInput ("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes ());
		let output = sha1.GetOutput ();
		assert! (hex::encode (output) == "84983e441c3bd26ebaae4aa1f95129e5e54670f1");
	}


	#[test]
	fn Sha2_224_1 ()
	{
		let mut sha2_224 = Sha2_224::New ();
		sha2_224.AddInput ("".as_bytes ());
		let output = sha2_224.GetOutput ();
		assert! (hex::encode (output) == "d14a028c2a3a2bc9476102bb288234c415a2b01f828ea62ac5b3e42f");
	}

	#[test]
	fn Sha2_224_2 ()
	{
		let mut sha2_224 = Sha2_224::New ();
		sha2_224.AddInput ("abc".as_bytes ());
		let output = sha2_224.GetOutput ();
		assert! (hex::encode (output) == "23097d223405d8228642a477bda255b32aadbce4bda0b3f7e36c9da7");
	}

	#[test]
	fn Sha2_224_3 ()
	{
		let mut sha2_224 = Sha2_224::New ();
		sha2_224.AddInput ("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes ());
		let output = sha2_224.GetOutput ();
		assert! (hex::encode (output) == "75388b16512776cc5dba5da1fd890150b0c6455cb4f58b1952522525");
	}


	#[test]
	fn Sha2_256_1 ()
	{
		let mut sha2_256 = Sha2_256::New ();
		sha2_256.AddInput ("".as_bytes ());
		let output = sha2_256.GetOutput ();
		assert! (hex::encode (output) == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
	}

	#[test]
	fn Sha2_256_2 ()
	{
		let mut sha2_256 = Sha2_256::New ();
		sha2_256.AddInput ("abc".as_bytes ());
		let output = sha2_256.GetOutput ();
		assert! (hex::encode (output) == "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad");
	}

	#[test]
	fn Sha2_256_3 ()
	{
		let mut sha2_256 = Sha2_256::New ();
		sha2_256.AddInput ("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes ());
		let output = sha2_256.GetOutput ();
		assert! (hex::encode (output) == "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1");
	}


	#[test]
	fn Sha2_384_1 ()
	{
		let mut sha2_384 = Sha2_384::New ();
		sha2_384.AddInput ("".as_bytes ());
		let output = sha2_384.GetOutput ();
		assert! (hex::encode (output) == "38b060a751ac96384cd9327eb1b1e36a21fdb71114be07434c0cc7bf63f6e1da274edebfe76f65fbd51ad2f14898b95b");
	}

	#[test]
	fn Sha2_384_2 ()
	{
		let mut sha2_384 = Sha2_384::New ();
		sha2_384.AddInput ("abc".as_bytes ());
		let output = sha2_384.GetOutput ();
		assert! (hex::encode (output) == "cb00753f45a35e8bb5a03d699ac65007272c32ab0eded1631a8b605a43ff5bed8086072ba1e7cc2358baeca134c825a7");
	}

	#[test]
	fn Sha2_384_3 ()
	{
		let mut sha2_384 = Sha2_384::New ();
		sha2_384.AddInput ("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes ());
		let output = sha2_384.GetOutput ();
		assert! (hex::encode (output) == "3391fdddfc8dc7393707a65b1b4709397cf8b1d162af05abfe8f450de5f36bc6b0455a8520bc4e6f5fe95b1fe3c8452b");
	}


	#[test]
	fn Sha2_512_1 ()
	{
		let mut sha2_512 = Sha2_512::New ();
		sha2_512.AddInput ("".as_bytes ());
		let output = sha2_512.GetOutput ();
		assert! (hex::encode (output) == "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e");
	}

	#[test]
	fn Sha2_512_2 ()
	{
		let mut sha2_512 = Sha2_512::New ();
		sha2_512.AddInput ("abc".as_bytes ());
		let output = sha2_512.GetOutput ();
		assert! (hex::encode (output) == "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f");
	}

	#[test]
	fn Sha2_512_3 ()
	{
		let mut sha2_512 = Sha2_512::New ();
		sha2_512.AddInput ("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes ());
		let output = sha2_512.GetOutput ();
		assert! (hex::encode (output) == "204a8fc6dda82f0a0ced7beb8e08a41657c16ef468b228a8279be331a703c33596fd15c13b1b07f9aa1d3bea57789ca031ad85c7a71dd70354ec631238ca3445");
	}
}

