use std::fmt::Debug;


pub fn Align<T> (input: T, align: usize) -> T
where T: Copy + std::convert::TryFrom<usize> + std::ops::Add<Output=T> + std::ops::Sub<Output=T> + std::ops::Mul<Output=T> + std::ops::Div<Output=T>, <T as TryFrom<usize>>::Error: Debug
{
	let align: T = align.try_into ().unwrap ();
	((input + align - 1.try_into ().unwrap ())/align)*align
}

pub fn AlignLeft<T> (input: T, align: usize) -> T
where T: Copy + std::convert::TryFrom<usize> + std::ops::Add<Output=T> + std::ops::Mul<Output=T> + std::ops::Div<Output=T>, <T as TryFrom<usize>>::Error: Debug
{
	let align: T = align.try_into ().unwrap ();
	(input/align)*align
}
