#![allow(non_snake_case)]					// turn off warning about snake_case name
#![allow(non_snake_case)]			// turn off warning about snake_case name
#![allow(non_camel_case_types)]		// turn off warning about upper camel case type name
#![allow(non_upper_case_globals)]	// turn off warning about global non upper camel name
#![allow(dead_code)]
#![feature(generic_const_exprs)]
#![feature(wrapping_int_impl)]


pub mod BlockCipher;
pub mod StreamCipher;
pub mod Hasher;
pub (in crate) mod Misc;


pub use BlockCipher::*;


pub fn Aes128BlockCipher (key: Aes128CipherKey, iv: [u8; Aes128Cipher::BLOCK_SIZE], blockMode: BlockCipher::BlockMode, paddingMode: BlockCipher::PaddingMode) -> BlockCipher::BlockCipher<Aes128Cipher>
{
	let aes128Cipher = Aes128Cipher::New (&key);
	BlockCipher::BlockCipher::New (aes128Cipher, blockMode, paddingMode, &iv)
}

pub fn Aes192BlockCipher (key: Aes192CipherKey, iv: [u8; Aes192Cipher::BLOCK_SIZE], blockMode: BlockCipher::BlockMode, paddingMode: BlockCipher::PaddingMode) -> BlockCipher::BlockCipher<Aes192Cipher>
{
	let aes192Cipher = Aes192Cipher::New (&key);
	BlockCipher::BlockCipher::New (aes192Cipher, blockMode, paddingMode, &iv)
}

pub fn Aes256BlockCipher (key: Aes256CipherKey, iv: [u8; Aes256Cipher::BLOCK_SIZE], blockMode: BlockCipher::BlockMode, paddingMode: BlockCipher::PaddingMode) -> BlockCipher::BlockCipher<Aes256Cipher>
{
	let aes256Cipher = Aes256Cipher::New (&key);
	BlockCipher::BlockCipher::New (aes256Cipher, blockMode, paddingMode, &iv)
}
