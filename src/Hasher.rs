pub mod MD5;
pub mod Sha;


pub trait HasherTrait
{
	const OUTPUT_SIZE: usize;

	fn AddInput (&mut self, input: &[u8]);
	fn GetOutput (self) -> [u8; Self::OUTPUT_SIZE];
	fn Hash (input: &[u8]) -> [u8; Self::OUTPUT_SIZE];
}
